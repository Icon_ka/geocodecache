package main

import "gitlab.com/Icon_ka/geocache/proxy/run"

func main() {
    app := run.NewApp()

    app.Bootstrap().Run()
}
