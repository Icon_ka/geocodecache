package run

import (
    "os"
    "net/http"
    "log"
    "time"
    "syscall"
    "os/signal"
    "context"
    "fmt"
    "github.com/joho/godotenv"
    _ "github.com/lib/pq"
    "gitlab.com/Icon_ka/geocache/proxy/internal/router"
    "gitlab.com/Icon_ka/geocache/proxy/internal/repository"
    "gitlab.com/Icon_ka/geocache/proxy/internal/service"
    "gitlab.com/Icon_ka/geocache/proxy/internal/entities"
    tabler2 "gitlab.com/Icon_ka/geocache/proxy/internal/tabler"
    "github.com/jmoiron/sqlx"
    "gitlab.com/Icon_ka/geocache/proxy/internal/controller"
    "gitlab.com/Icon_ka/geocache/proxy/internal/infrastructure/responder"
    "gitlab.com/Icon_ka/geocache/proxy/internal/utils"
    "go.uber.org/zap"
)

// Application - интерфейс приложения
type Application interface {
    Runner
    Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
    Run()
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
    Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
    srv      *http.Server
    Repo     *repository.AddressRepository
    Services *service.AddressServicer
}

func NewApp() *App {
    return &App{}
}

// Run - запуск приложения
func (a *App) Run() {
    go func() {
        log.Println("Starting server...")
        if err := a.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
            log.Fatalf("Server error: %v", err)
        }
    }()

    sigChan := make(chan os.Signal, 1)
    signal.Notify(sigChan, syscall.SIGINT)

    <-sigChan

    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    err := a.srv.Shutdown(ctx)
    if err != nil {
        log.Fatalf("Server shutdown error: %v", err)
    }

    <-ctx.Done()

    log.Println("Server stopped gracefully")
}

func (a *App) Bootstrap(options ...interface{}) Runner {
    err := godotenv.Load(".env")
    if err != nil {
        log.Fatal("Ошибка при загрузке файла .env")
    }
    db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
        os.Getenv("DB_USER"),
        os.Getenv("DB_PASSWORD"),
        os.Getenv("DB_HOST"),
        os.Getenv("DB_PORT"),
        os.Getenv("DB_NAME"),
    ))
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    var generator tabler2.SQLiteGenerator
    m := tabler2.NewMigrator(db, &generator)
    err = m.Migrate(&entities.Address{}, &entities.SearchHistory{}, &entities.HistorySearchAddress{})
    if err != nil {
        panic(err)
    }

    logger := zap.NewExample()
    dadata := utils.NewDadata()
    resp := responder.NewResponder(logger)
    serv := service.NewAddressService(db)
    ctrl := controller.NewControllers(resp, dadata, serv)
    r := router.NewApiRouter(ctrl)

    // Создание HTTP-сервера
    a.srv = &http.Server{
        Addr:         ":8080",
        Handler:      r,
        ReadTimeout:  10 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

    return a
}
