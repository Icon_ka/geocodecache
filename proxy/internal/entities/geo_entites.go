package entities

// SearchRequest model info
// @Description Запрос для поиска адреса
type SearchRequest struct {
    // Адрес
    Query string `json:"query"`
}

// GeocodeRequest model info
// @Description Запрос для геокодирования
type GeocodeRequest struct {
    // Широта
    Lat string `json:"lat"`
    // Долгота
    Lon string `json:"lon"`
}

type SearchHistory struct {
    ID    int    `db:"id" db_type:"SERIAL PRIMARY KEY"`
    Query string `db:"query" db_type:"VARCHAR(255)"`
}

func (h SearchHistory) TableName() string {
    return "search_history"
}

type Address struct {
    ID   int    `db:"id" db_type:"SERIAL PRIMARY KEY" json:"id"`
    Data string `db:"data" db_type:"VARCHAR(255)" json:"data"`
}

func (a Address) TableName() string {
    return "address"
}

type HistorySearchAddress struct {
    SearchHistoryID int `db:"search_history_id" db_type:"INTEGER"`
    AddressID       int `db:"address_id" db_type:"INTEGER"`
}

func (h HistorySearchAddress) TableName() string {
    return "history_search_address"
}

// Addresses model info
// @Description Слайс адресов
type Addresses struct {
    // Слайс адресов
    Suggestions []Suggestion `json:"suggestions"`
}

// Suggestion model info
// @Description Информация о адресе
// @Description Адрес, полный адрес и координаты
type Suggestion struct {
    // Адрес
    Value string `json:"value"`
    // Полный адрес
    UnrestrictedValue string `json:"unrestricted_value"`
    // Координаты
    Data Data `json:"data"`
}

// Data model info
// @Description Координаты
// @Description Широта и долгота
type Data struct {
    // Широта
    Lat string `json:"geo_lat"`
    // Долгота
    Lon string `json:"geo_lon"`
}
