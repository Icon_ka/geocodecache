package geo

import (
    "encoding/json"
    "errors"
    "net/http"
    "strings"
    "fmt"
    "gitlab.com/Icon_ka/geocache/proxy/internal/service"
    "gitlab.com/Icon_ka/geocache/proxy/internal/infrastructure/responder"
    "gitlab.com/Icon_ka/geocache/proxy/internal/entities"
    "gitlab.com/Icon_ka/geocache/proxy/internal/utils"
)

type GeoCodeController struct {
    Responder    responder.Responder
    DadataClient utils.Dadaterer
    Service      service.AddressServicer
}

func NewGeoCodeController(responder responder.Responder, dadaterer utils.Dadaterer, serv service.AddressServicer) *GeoCodeController {
    return &GeoCodeController{
        Responder:    responder,
        DadataClient: dadaterer,
        Service:      serv,
    }
}

// Geocode godoc
// @Summary     Геокодирование адреса
// @Description Геокодирование адреса
// @Tags        geocode
// @Accept      json
// @Produce     json
// @Param       body body entities.GeocodeRequest true "Запрос на поиск адресов"
// @Success     200 {object} entities.Addresses "Успешный ответ"
// @Router      /api/address/geocode [post]
func (g *GeoCodeController) Geocode(w http.ResponseWriter, r *http.Request) {
    var re entities.GeocodeRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при сериализации JSON"))
        return
    }

    jsonData, err := json.Marshal(re)
    var data = strings.NewReader(string(jsonData))

    resp, err := g.DadataClient.Geocode(data)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при десериализации JSON"))
        return
    }
}

// Search godoc
// @Summary     Поиск адресов
// @Description Поиск адресов по запросу
// @Tags        geocode
// @Accept      json
// @Produce     json
// @Param       body body entities.SearchRequest true "Запрос на поиск адресов"
// @Success     200 {object} entities.Addresses "Успешный ответ"
// @Router      /api/address/search [post]
func (g *GeoCodeController) Search(w http.ResponseWriter, r *http.Request) {
    var re entities.SearchRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при сериализации JSON"))
        return
    }

    resp, err := g.Service.GetAddresses(re)
    if err != nil {
        fmt.Println("Service: ", err)
        g.Responder.ErrorInternal(w, errors.New("Ошибка при запросе к базе данных"))
        return
    }

    fmt.Println(resp)
    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при десериализации JSON"))
        return
    }
}
