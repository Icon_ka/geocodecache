package controller

import (
    "gitlab.com/Icon_ka/geocache/proxy/internal/service"
    geo2 "gitlab.com/Icon_ka/geocache/proxy/internal/controller/geo"
    "gitlab.com/Icon_ka/geocache/proxy/internal/infrastructure/responder"
    "gitlab.com/Icon_ka/geocache/proxy/internal/utils"
)

type Controllers struct {
    Geocode geo2.GeoCoder
}

func NewControllers(responder responder.Responder, dadaterer utils.Dadaterer, serv service.AddressServicer) *Controllers {
    geocodeController := geo2.NewGeoCodeController(responder, dadaterer, serv)
    return &Controllers{
        Geocode: geocodeController,
    }
}
