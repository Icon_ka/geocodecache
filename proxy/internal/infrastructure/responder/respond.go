package responder

import (
    "net/http"
    "context"

    "go.uber.org/zap"
    "encoding/json"
    "errors"
    "gitlab.com/Icon_ka/geocache/proxy/internal/infrastructure/response"
)

type Responder interface {
    OutputJSON(w http.ResponseWriter, responseData interface{})

    ErrorUnauthorized(w http.ResponseWriter, err error)
    ErrorBadRequest(w http.ResponseWriter, err error)
    ErrorForbidden(w http.ResponseWriter, err error)
    ErrorInternal(w http.ResponseWriter, err error)
}

type Respond struct {
    log *zap.Logger
}

func NewResponder(logger *zap.Logger) Responder {
    return &Respond{log: logger}
}

func (r *Respond) OutputJSON(w http.ResponseWriter, responseData interface{}) {
    w.Header().Set("Content-Type", "application/json;charset=utf-8")
    err := json.NewEncoder(w).Encode(responseData)
    if err != nil {
        r.log.Error("responder json encode error", zap.Error(err))
    }
}

func (r *Respond) ErrorBadRequest(w http.ResponseWriter, err error) {
    r.log.Info("http response bad request status code", zap.Error(err))
    w.Header().Set("Content-Type", "application/json;charset=utf-8")
    w.WriteHeader(http.StatusBadRequest)
    err = json.NewEncoder(w).Encode(response.Response{
        Success: false,
        Message: err.Error(),
        Data:    nil,
    })
    if err != nil {
        r.log.Info("response writer error on write", zap.Error(err))
    }
}

func (r *Respond) ErrorForbidden(w http.ResponseWriter, err error) {
    r.log.Warn("http resposne forbidden", zap.Error(err))
    w.Header().Set("Content-Type", "application/json;charset=utf-8")
    w.WriteHeader(http.StatusForbidden)
    err = json.NewEncoder(w).Encode(response.Response{
        Success: false,
        Message: err.Error(),
        Data:    nil,
    })
    if err != nil {
        r.log.Error("response writer error on write", zap.Error(err))
    }
}

func (r *Respond) ErrorUnauthorized(w http.ResponseWriter, err error) {
    r.log.Warn("http resposne Unauthorized", zap.Error(err))
    w.Header().Set("Content-Type", "application/json;charset=utf-8")
    w.WriteHeader(http.StatusUnauthorized)
    err = json.NewEncoder(w).Encode(response.Response{
        Success: false,
        Message: err.Error(),
        Data:    nil,
    })
    if err != nil {
        r.log.Error("response writer error on write", zap.Error(err))
    }
}

func (r *Respond) ErrorInternal(w http.ResponseWriter, err error) {
    if errors.Is(err, context.Canceled) {
        return
    }
    r.log.Error("http response internal error", zap.Error(err))
    w.Header().Set("Content-Type", "application/json;charset=utf-8")
    w.WriteHeader(http.StatusInternalServerError)
    err = json.NewEncoder(w).Encode(response.Response{
        Success: false,
        Message: err.Error(),
        Data:    nil,
    })
    if err != nil {
        r.log.Error("response writer error on write", zap.Error(err))
    }
}
