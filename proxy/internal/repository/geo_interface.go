package repository

type AddressRepository interface {
    GetAddresses(id int) (string, error)
    GetIdFromQuery(query string) (int, error)
    GetHistoryIdFromId(id int) (int, error)
    SaveSearchHistory(query string) (int, error)
    SaveAddress(data string) (int, error)
    SaveHistorySearchAddress(searchHistoryID, addressID int) error
}
