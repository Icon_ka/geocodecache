package repository

import (
    "github.com/jmoiron/sqlx"
    sq "github.com/Masterminds/squirrel"
    "database/sql"
)

type PostgresAddressRepository struct {
    db      *sqlx.DB
    builder sq.StatementBuilderType
}

func NewPostgresAddressRepository(db *sqlx.DB) *PostgresAddressRepository {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
    return &PostgresAddressRepository{db: db, builder: builder}
}

func (r *PostgresAddressRepository) GetAddresses(id int) (string, error) {
    q := `SELECT data FROM address WHERE id = $1`

    var data string
    err := r.db.QueryRow(q, id).Scan(&data)
    if err != nil {
        if err == sql.ErrNoRows {
            return "", nil
        }
        return "", err
    }

    return data, nil
}

func (r *PostgresAddressRepository) GetIdFromQuery(query string) (int, error) {
    q := `SELECT id FROM search_history WHERE similarity(query, $1) >= 0.7`

    var id int
    err := r.db.QueryRow(q, query).Scan(&id)
    if err != nil {
        if err == sql.ErrNoRows {
            return 0, nil
        }
        return 0, err
    }

    return id, nil
}

func (r *PostgresAddressRepository) GetHistoryIdFromId(id int) (int, error) {
    q := `SELECT address_id FROM history_search_address WHERE search_history_id = $1`

    var historyId int
    err := r.db.QueryRow(q, id).Scan(&historyId)
    if err != nil {
        if err == sql.ErrNoRows {
            return 0, nil
        }
        return 0, err
    }

    return historyId, nil
}

func (r *PostgresAddressRepository) SaveSearchHistory(query string) (int, error) {
    q := `INSERT INTO search_history (query) VALUES ($1) RETURNING id`

    var id int
    err := r.db.QueryRow(q, query).Scan(&id)
    if err != nil {
        return 0, err
    }

    return id, nil
}

func (r *PostgresAddressRepository) SaveAddress(data string) (int, error) {
    query := `INSERT INTO address (data) VALUES ($1) RETURNING id`

    var id int
    err := r.db.QueryRow(query, data).Scan(&id)
    if err != nil {
        return 0, err
    }
    return id, nil
}

func (r *PostgresAddressRepository) SaveHistorySearchAddress(searchHistoryID, addressID int) error {
    query := `INSERT INTO history_search_address (search_history_id, address_id) VALUES ($1, $2)`

    _, err := r.db.Exec(query, searchHistoryID, addressID)
    if err != nil {
        return err
    }
    return nil
}
