package utils

import (
    "net/http"
    "strings"
    "encoding/json"
    "gitlab.com/Icon_ka/geocache/proxy/internal/entities"
)

const (
    search  = "/suggest/address"
    geocode = "/geolocate/address"
)

type Dadaterer interface {
    Search(*strings.Reader) (entities.Addresses, error)
    Geocode(*strings.Reader) (entities.Addresses, error)
}

type Dadata struct {
    client *http.Client
    url    string
}

func NewDadata(opts ...func(exmo *Dadata)) *Dadata {
    dadata := &Dadata{
        client: http.DefaultClient,
        url:    "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
    }
    for _, opt := range opts {
        opt(dadata)
    }

    return dadata
}

func (d *Dadata) Search(query *strings.Reader) (entities.Addresses, error) {
    req, err := http.NewRequest("POST", d.url+search, query)
    if err != nil {
        return entities.Addresses{}, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := d.client.Do(req)
    if err != nil {
        return entities.Addresses{}, err
    }
    defer resp.Body.Close()

    var address entities.Addresses
    err = json.NewDecoder(resp.Body).Decode(&address)
    if err != nil {
        return entities.Addresses{}, err
    }

    return address, nil
}

func (d *Dadata) Geocode(query *strings.Reader) (entities.Addresses, error) {
    req, err := http.NewRequest("POST", d.url+geocode, query)
    if err != nil {
        return entities.Addresses{}, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := d.client.Do(req)
    if err != nil {
        return entities.Addresses{}, err
    }
    defer resp.Body.Close()

    var address entities.Addresses
    err = json.NewDecoder(resp.Body).Decode(&address)
    if err != nil {
        return entities.Addresses{}, err
    }

    return address, nil
}
