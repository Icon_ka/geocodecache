package service

import (
    "github.com/jmoiron/sqlx"
    "strings"
    "encoding/json"
    repository2 "gitlab.com/Icon_ka/geocache/proxy/internal/repository"
    "gitlab.com/Icon_ka/geocache/proxy/internal/entities"
    "gitlab.com/Icon_ka/geocache/proxy/internal/utils"
)

type AddressService struct {
    pg     repository2.AddressRepository
    dadata utils.Dadaterer
}

func NewAddressService(pg *sqlx.DB) *AddressService {
    return &AddressService{pg: repository2.NewPostgresAddressRepository(pg), dadata: utils.NewDadata()}
}
func (a AddressService) GetAddresses(req entities.SearchRequest) (entities.Addresses, error) {
    var addr entities.Addresses
    id, err := a.pg.GetIdFromQuery(req.Query)
    if err != nil {
        return entities.Addresses{}, err
    }

    if id == 0 {
        jsonData, err := json.Marshal(req)
        if err != nil {
            return entities.Addresses{}, err
        }
        var data = strings.NewReader(string(jsonData))

        addr, err = a.dadata.Search(data)
        if err != nil {
            return entities.Addresses{}, err
        }

        idSearch, err := a.pg.SaveSearchHistory(req.Query)
        if err != nil {
            return entities.Addresses{}, err
        }

        id, err := a.pg.SaveAddress(addr.Suggestions[0].Value)
        if err != nil {
            return entities.Addresses{}, err
        }

        err = a.pg.SaveHistorySearchAddress(idSearch, id)
        if err != nil {
            return entities.Addresses{}, err
        }

        return addr, nil
    }

    historyId, err := a.pg.GetHistoryIdFromId(id)
    if err != nil {
        return entities.Addresses{}, err
    }

    data, err := a.pg.GetAddresses(historyId)
    if err != nil {
        return entities.Addresses{}, err
    }

    s := make([]entities.Suggestion, 1)
    if len(data) != 0 {
        s[0].Value = data
        addr.Suggestions = s
    }

    return addr, nil
}
