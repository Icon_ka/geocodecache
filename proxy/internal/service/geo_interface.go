package service

import (
    "geocache/proxy/internal/entities"
)

type AddressServicer interface {
    GetAddresses(req entities.SearchRequest) (entities.Addresses, error)
}
