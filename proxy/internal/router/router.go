package router

import (
    "github.com/go-chi/chi"

    "net/http"
    httpSwagger "github.com/swaggo/http-swagger"
    _ "gitlab.com/Icon_ka/geocache/proxy/docs"
    modules "gitlab.com/Icon_ka/geocache/proxy/internal/controller"
)

func NewApiRouter(controllers *modules.Controllers) http.Handler {
    r := chi.NewRouter()

    r.Get("/swagger/*", httpSwagger.Handler(
        httpSwagger.URL("http://localhost:8080/swagger/doc.json"), //The url pointing to API definition
    ))

    r.Post("/api/address/search", controllers.Geocode.Search)

    r.Post("/api/address/geocode", controllers.Geocode.Geocode)

    return r
}
