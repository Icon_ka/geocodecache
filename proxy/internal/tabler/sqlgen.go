package tabler

import (
    "fmt"
    "reflect"
    "strings"
)

type SQLGenerator interface {
    CreateTableSQL(table Tabler) string
}

type SQLiteGenerator struct{}

func (sg *SQLiteGenerator) CreateTableSQL(table Tabler) string {
    query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (\n", table.TableName())
    fields := getFields(table)
    for i, field := range fields {
        query += fmt.Sprintf(" %s %s,\n", GetStructInfo(table).Fields[i], field.Tag.Get("db_type"))
    }
    query = strings.TrimSuffix(query, ",\n")
    query += "\n);"

    return query
}

func getFields(v interface{}) []reflect.StructField {
    t := reflect.TypeOf(v)
    if t.Kind() == reflect.Ptr {
        t = t.Elem()
    }
    fields := make([]reflect.StructField, 0)
    for i := 0; i < t.NumField(); i++ {
        field := t.Field(i)
        fields = append(fields, field)
    }
    return fields
}
