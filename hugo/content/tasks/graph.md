---
menu:
    after:
        name: graph
        weight: 1
title: Построение графа
---


{{< mermaid >}} 
 graph LR
node1{Rhombus} --> node2((Circle))
node1{Rhombus} --> node10[Square]
node1{Rhombus} --> node12[Rect]
node1{Rhombus} --> node27(Ellipse)
node1{Rhombus} --> node28(Round Rect)
node2((Circle)) --> node1{Rhombus}
node2((Circle)) --> node3(Ellipse)
node2((Circle)) --> node5[Square]
node2((Circle)) --> node6(Round Rect)
node2((Circle)) --> node8[Rect]
node2((Circle)) --> node19(Round Rect)
node3(Ellipse) --> node2((Circle))
node3(Ellipse) --> node4{Rhombus}
node3(Ellipse) --> node15{Rhombus}
node4{Rhombus} --> node3(Ellipse)
node5[Square] --> node2((Circle))
node5[Square] --> node11{Rhombus}
node6(Round Rect) --> node2((Circle))
node6(Round Rect) --> node7((Circle))
node6(Round Rect) --> node16{Rhombus}
node6(Round Rect) --> node21[Rect]
node6(Round Rect) --> node26(Round Rect)
node7((Circle)) --> node6(Round Rect)
node7((Circle)) --> node9[Rect]
node7((Circle)) --> node23{Rhombus}
node8[Rect] --> node2((Circle))
node8[Rect] --> node13(Round Rect)
node9[Rect] --> node7((Circle))
node9[Rect] --> node20[Rect]
node9[Rect] --> node25{Rhombus}
node10[Square] --> node1{Rhombus}
node11{Rhombus} --> node5[Square]
node11{Rhombus} --> node14[Rect]
node11{Rhombus} --> node22(Ellipse)
node12[Rect] --> node1{Rhombus}
node12[Rect] --> node17[Rect]
node13(Round Rect) --> node8[Rect]
node14[Rect] --> node11{Rhombus}
node14[Rect] --> node18(Round Rect)
node15{Rhombus} --> node3(Ellipse)
node16{Rhombus} --> node6(Round Rect)
node17[Rect] --> node12[Rect]
node18(Round Rect) --> node14[Rect]
node19(Round Rect) --> node2((Circle))
node20[Rect] --> node9[Rect]
node20[Rect] --> node29(Round Rect)
node21[Rect] --> node6(Round Rect)
node22(Ellipse) --> node11{Rhombus}
node22(Ellipse) --> node24((Circle))
node23{Rhombus} --> node7((Circle))
node24((Circle)) --> node22(Ellipse)
node25{Rhombus} --> node9[Rect]
node26(Round Rect) --> node6(Round Rect)
node27(Ellipse) --> node1{Rhombus}
node28(Round Rect) --> node1{Rhombus}
node29(Round Rect) --> node20[Rect]

{{< /mermaid >}}
