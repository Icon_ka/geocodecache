---
menu:
    after:
        name: binary_tree
        weight: 2
title: Построение сбалансированного бинарного дерева
---


{{< mermaid >}} 
 graph TD
81 --> 33
33 --> 59
81 --> 847
847 --> 402
402 --> 289
402 --> 633
847 --> 887

{{< /mermaid >}}
